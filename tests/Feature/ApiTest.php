<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Objects;

class ApiTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    #region create
    public function testCreate()
    {
        $data = [
            "mykey" => "test key",
        ];

        $this->json('POST', '/api/object', $data, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                "message" => "success",
                "error" => ""
            ]);
    }

    public function testRequiredCreate()
    {
        $this->json('POST', '/api/object', [], ['Accept' => 'application/json'])
            ->assertStatus(404)
            ->assertJson([
                "message" => "required json with one key and one value",
                "error" => "Bad request"
            ]);
    }

    #endregion

    #region Get Data
    public function testGetKey(){
        $object = Objects::create(["data_key" => "mykey", "key_value" => "test key"]);
        $this->json('GET', '/api/object/mykey', [], ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                "message" => "success",
                "data" => ["mykey" => "test key"]
            ]);
    }

    public function testGetAllKey(){
        $object = Objects::create(["data_key" => "mykey", "key_value" => "test key"]);
        $this->json('GET', '/api/object/get_all_records/', [], ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                "message" => "success",
                "error" => "",
                "data" => [
                    [
                        "dataKey" => "mykey",
                        "value" => "test key"
                    ]
                ]
            ]);
    }

    public function testGetByTime(){
        $object = Objects::create(["data_key" => "mykey", "key_value" => "test key"]);
        $this->json('GET', '/api/object/mykey?timestamp='.time(), [], ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                "message" => "success",
                "error" => "",
                "data" => [
                    "mykey" => "test key"
                ]
            ]);
    }
    #end region


    public function test_example()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }
}
