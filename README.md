1. Accept a key(string) and value(some JSON blob/string) and store them. If an
   existing key is sent, the value should be updated <br/>
   Method: POST <br/>
   Endpoint: http://35.194.186.249/api/object <br/>
   Body: JSON: {mykey : bbb} <br/>
   Response: <br/>
   {"error":"","data":{"mykey":"aaa","time":1621746311},"message":"success"} <br/>
   <br/>
   
2. Accept a key and return the corresponding latest value <br/>
   Method: GET<br/>
   Endpoint: http://35.194.186.249/api/object/mykey <br/>
   Response: <br/>
   {"error":"","data":{"mykey":"bbb"},"message":"success"} <br/>
    <br/>
   
3. When given a key AND a timestamp, return whatever the value of the key at the
   time was.<br/>
   Method: GET <br/>
   Endpoint: http://35.194.186.249/api/object/mykey?timestamp=1621746311
   Response: <br/>
   {"error":"","data":{"mykey":"aaa"},"message":"success"}<br/>
    <br/>
4. Displays all values currently stored in the database.
   Method: GET <br/>
   Endpoint: http://35.194.186.249/api/object/get_all_records <br/>
   Response: <br/>
   {"error":"","data":[{"dataKey":"mykey","value":"bbb","timestamp":1621746711},{"dataKey":"mykey","value":"aaa","timestamp":1621746311}],"message":"success"}
