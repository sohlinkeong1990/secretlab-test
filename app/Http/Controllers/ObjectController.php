<?php

namespace App\Http\Controllers;

use App\Models\Objects;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use mysql_xdevapi\Exception;
use Illuminate\Support\Facades\DB;

class ObjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $data = $request->all();
        $keys = array_keys($data);

        if ((count($data) < 1) || (count($keys) < 1) || (!is_string($data[$keys[0]])) || (empty(trim($keys[0]))) ) {
            return response()->json([
                'error' => 'Bad request',
                'message' => "required json with one key and one value"
            ], 404);
        }

        $input['data_key'] = $keys[0];
        $input['key_value'] = $data[$keys[0]];
        if ($input['data_key'] == "get_all_records") {
            return response()->json([
                'error' => 'Bad request',
                'message' => "Invalid key"
            ], 404);
        }

        if ($object = Objects::create($input)) {

            $data['time'] = $object->created_at->getTimestamp();
            return response()->json([
                'error' => '',
                'data' => $data,
                'message' => 'success'
            ]);
        }


    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($key)
    {

        if ($key == "get_all_records") {
            $objects = Objects::select(DB::raw(' data_key as dataKey, key_value as value, UNIX_TIMESTAMP(created_at) as timestamp'))->orderBy('created_at', 'desc')->get();

            if (count($objects) > 0) {
                return response()->json([
                    'error' => '',
                    'data' => $objects,
                    'message' => 'success'
                ]);
            }


        } else {
            $object = null;
            $query = Objects::select('data_key', 'key_value')->where('data_key', $key);
            if (isset($_GET['timestamp'])) {
                if ((is_numeric($_GET['timestamp']) && (int)$_GET['timestamp'] == $_GET['timestamp'])) {
                    $object = $query->where('created_at', '<=', gmdate("Y-m-d H:i:s", $_GET['timestamp']))->orderBy('created_at', 'desc')->limit(1)->get();
                } else {
                    return response()->json([
                        'error' => 'Bad request',
                        'message' => "invalid timestamp"
                    ], 404);
                }
            } else {
                $object = $query->latest()->limit(1)->get();
            }

            if (!$object->isEmpty()) {
                $data[$object[0]->data_key] = $object[0]->key_value;
                return response()->json([
                    'error' => '',
                    'data' => $data,
                    'message' => 'success'
                ]);
            }
        }

        return response()->json([
            'error' => '',
            'data' => null,
            'message' => 'no result found'
        ]);


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function getAllRecords()
    {

    }
}
